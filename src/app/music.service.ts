import { Injectable } from '@angular/core';
import { SheetMusic } from './Models/SheetMusic';
import * as fs from "file-system"; 


export interface MusicFolder {
  location: string;
  title: string;
  contents: SheetMusic[];
}


@Injectable()
export class MusicService {
  availableMusic: SheetMusic[] = [
    {
      location: '../../SheetMusic/1.14 SteelHarpBandFinal - Parts.pdf',
      title: 'Steel Harp - Parts',
      id: 1
    },
    {
      location: '../../SheetMusic/1.14 SteelHarpBandFinal - Full Score.pdf',
      title: 'Steel Harp - Full Score',
      id: 2
    }
  ];
  availableFolders: MusicFolder[] = [
    {
      location: '../../SheetMusic/Waterman Junction',
      title: "WaterMan Junction",
      contents: [
        {
          location: '../../SheetMusic/Waterman Junction/I The 3751 2015 - Full Score.pdf',
          title: "I The 3751 2015 - Full Score",
          id: 3
        },
        {
          location: '../../SheetMusic/Waterman Junction/I The 3751 2015 - Parts.pdf',
          title: "I The 3751 2015 - Parts",
          id: 4
        }
      ]

    },
    {
      location: '../../SheetMusic/SteelHarpParts',
      title: "Steel Harp", 
      contents: this.availableMusic
    }
  ];


  constructor() { }

  getMusicFolderList() {
    return this.availableFolders;
  }

  getMusicList(folder: string) {
    return this.availableFolders.find(f => f.title === folder).contents;
  }

  getMusic(id: number) : SheetMusic {
    for ( let folder of this.availableFolders){ 
      var result = folder.contents.find(music => music.id == id);
      if(result) { return result; }
    }
  }

}

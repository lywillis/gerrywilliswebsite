import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {RouterModule, Routes} from '@angular/router';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { CatalogueComponent } from './catalogue/catalogue.component';
import { SheetMusicComponent } from './catalogue/sheet-music/sheet-music.component';
import {MusicService} from './music.service'; 


import { PdfViewerModule } from 'ng2-pdf-viewer';
import { FolderContentsComponent } from './catalogue/folder-contents/folder-contents.component';


const routes: Routes = [
{path: '', redirectTo: 'home', pathMatch: 'full'}, 
{path: 'home', component: HomeComponent}, 
{path: 'about', component: AboutComponent}, 
{path: 'catalogue', component: CatalogueComponent}, 
{path: 'folderContents/:folder', component: FolderContentsComponent}, 
{path: 'sheetMusic/:id', component: SheetMusicComponent}

]
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    CatalogueComponent,
    SheetMusicComponent,
    FolderContentsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(routes),
    PdfViewerModule
  ],
  providers: [MusicService],
  bootstrap: [AppComponent]
})
export class AppModule { }


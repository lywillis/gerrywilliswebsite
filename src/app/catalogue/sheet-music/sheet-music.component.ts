import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import {SheetMusic} from '../../Models/SheetMusic';
import {MusicService} from '../../music.service'; 

@Component({
  selector: 'sheet-music-preview',
  templateUrl: './sheet-music.component.html',
  styleUrls: ['./sheet-music.component.css']
})
export class SheetMusicComponent implements OnInit {
  id: number;
  sheetMusic: SheetMusic;  

  showPreview: boolean; 
  
  constructor(private route: ActivatedRoute, 
    private router: Router,
  private musicService: MusicService) {
    route.params.subscribe(params => {this.id = params['id'];})
    
  }

   
  ngOnInit() {
    this.showPreview = false; 
    this.sheetMusic = this.musicService.getMusic(this.id); 
    console.log(this.sheetMusic); 
    
  }

  GetPreview() {
    this.showPreview = true; 

  }

}

import { Component, OnInit } from '@angular/core';
import { SheetMusic } from '../Models/SheetMusic';
import { Router, ActivatedRoute } from '@angular/router';
import { MusicFolder, MusicService } from '../music.service';


@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.css']
})
export class CatalogueComponent implements OnInit {
  availableFolders: MusicFolder[];

  constructor(private router: Router,
    private route: ActivatedRoute,
    private musicService: MusicService) {
  }

  ngOnInit() {

    this.availableFolders = this.musicService.getMusicFolderList();

  }



}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { SheetMusic } from '../../Models/SheetMusic';
import { MusicService } from '../../music.service';


@Component({
  selector: 'folder-contents',
  templateUrl: './folder-contents.component.html',
  styleUrls: ['../catalogue.component.css']
})
export class FolderContentsComponent implements OnInit {
  currentFolder: string;
  availableMusic: SheetMusic[];

  constructor(private router: Router,
    private route: ActivatedRoute,
    private musicService: MusicService) {
      route.params.subscribe(params => {
        {

          this.currentFolder = params['folder'];
          

        };
      }
      )
  }

  ngOnInit() {
    this.availableMusic = this.musicService.getMusicList(this.currentFolder); 
  }

}

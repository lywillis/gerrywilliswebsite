import { GerryWillisPage } from './app.po';

describe('gerry-willis App', () => {
  let page: GerryWillisPage;

  beforeEach(() => {
    page = new GerryWillisPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
